﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SunnysAdventure.Class.Collision
{
    interface ICollide
    {
        Rectangle GetCollisionRectangle();
        Color[] GetTextureColorData();

    }
}
