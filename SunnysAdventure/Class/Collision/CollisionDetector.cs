﻿using Microsoft.Xna.Framework;
using SunnysAdventure.Class.MovebleObj;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SunnysAdventure.Class.Collision
{
    class CollisionDetector
    {
        public static bool PixelCollisionDetector(MovebleObject movebleObject, ICollide collideObject)
        {
            Rectangle rectangleA = movebleObject.GetCollisionRectangle();
            Color[] dataA = movebleObject.GetTextureColorData();
            Rectangle rectangleB = collideObject.GetCollisionRectangle();
            Color[] dataB = collideObject.GetTextureColorData();

            if (rectangleA.Intersects(rectangleB))
            {
                // 先找到 兩個圖形的矩形(Rectangle) 的 交疊區域
                int overlap_top = Math.Max(rectangleA.Top, rectangleB.Top);  // y值，螢幕越下面越大
                int overlap_bottom = Math.Min(rectangleA.Bottom, rectangleB.Bottom);
                int overlap_left = Math.Max(rectangleA.Left, rectangleB.Left);
                int overlap_right = Math.Min(rectangleA.Right, rectangleB.Right);

                // 再搜尋 交疊區域中 雙方圖形 的 每一個像素值
                for (int y = overlap_top; y < overlap_bottom; ++y)
                {
                    for (int x = overlap_left; x < overlap_right; ++x)
                    {
                        // 雙方相交的那個圖素點 的顏色資料
                        Color colorA = dataA[(y - rectangleA.Top) * rectangleA.Width + (x - rectangleA.Left)];
                        Color colorB = dataB[(y - rectangleB.Top) * rectangleB.Width + (x - rectangleB.Left)];

                        // 如果 雙方相交的那個圖素點 都不是透明
                        if (colorA.A != 0 && colorB.A != 0) // .A: alpha值 
                        {
                            movebleObject.isCollide = true;
                            return true; // 有圖素點 的交疊
                        }
                    }
                }
            }
           
            return false; // 無 圖素點 的交疊

           
        }
        public static void RectangleCollisionDetector(MovebleObject movebleObject, ICollide collideObject)
        {
            if (movebleObject.GetCollisionRectangle().Intersects(collideObject.GetCollisionRectangle()))
            {
                movebleObject.isCollide = true;
                movebleObject.IntersectRectangle = Rectangle.Intersect(movebleObject.GetCollisionRectangle(), collideObject.GetCollisionRectangle());

            }
            else
            {
                movebleObject.isCollide = false;
                movebleObject.IntersectRectangle = new Rectangle();

            }
        }
        private void JumpCollisionDetector(MovebleObject movebleObject, ICollide collideObject)
        {
            if (movebleObject.isCollide)
            {
                movebleObject.IntersectRectangle = Rectangle.Intersect(movebleObject.GetCollisionRectangle(), collideObject.GetCollisionRectangle());
            }
            else
            {
                movebleObject.IntersectRectangle = new Rectangle();
            }
        }
    }
}
