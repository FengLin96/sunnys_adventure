﻿using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SunnysAdventure.Class.BedieningClass
{
     class Bediening
    {
        public bool Left { get; set; }
        public bool Right { get; set; }
        public bool Down { get; set; }
        public bool Jump { get; set; }
        //public bool Run { get; set; }
        public bool isKeyNotDown { get; set; }
        public void Update()
        {
            KeyboardState stateKey = Keyboard.GetState();

            Array x = stateKey.GetPressedKeys();
            if (x.Length == 0)
            {
                isKeyNotDown = true;
            }
            else
            {
                isKeyNotDown = false;
            }

            if (stateKey.IsKeyDown(Keys.Left))
            {
                Left = true;
            }
            else
            {
                Left = false;
            }

            if (stateKey.IsKeyDown(Keys.Right))
            {
                Right = true;
            }
            else
            {
                Right = false;
            }

            if (stateKey.IsKeyDown(Keys.Down))
            {
                Down = true;
            }
            else
            {
                Down = false;
            }

            if (stateKey.IsKeyDown(Keys.Up))
            {
                Jump = true;
            }
            else
            {
                Jump = false;
            }

        }



    }

}
