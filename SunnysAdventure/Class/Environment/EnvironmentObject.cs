﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SunnysAdventure.Class.Environment
{
    class EnvironmentObject:GameObject,IVisible
    {
        public Texture2D ObjectTexture { get; set; }

        public virtual void Show(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(ObjectTexture, Position, Color.AliceBlue);
        }

        public virtual void Update(GameTime gameTime)
        {
            throw new NotImplementedException();
        }
    }
}
