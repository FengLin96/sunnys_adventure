﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using SunnysAdventure.Class.Collision;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SunnysAdventure.Class.Environment
{
    class PlatformLong : EnvironmentObject, ICollide
    {
        public PlatformLong(ContentManager contente, Vector2 position)
        {
            this.ObjectTexture = contente.Load<Texture2D>("platform-long");
        }
        static public int TextureWidth
        {
            get
            {
                return 32;
            }
        }
        static public int TextureHeight
        {
            get
            {
                return 16;
            }
        }
        public Rectangle GetCollisionRectangle()
        {
            throw new NotImplementedException();
        }

        public Color[] GetTextureColorData()
        {
            throw new NotImplementedException();
        }
    }
}
