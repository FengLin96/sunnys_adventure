﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using SunnysAdventure.Class.Collision;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SunnysAdventure.Class.Environment
{
    class GBlockGroot : EnvironmentObject, ICollide
    {
        public GBlockGroot(ContentManager contente, Vector2 position)
        {
            this.ObjectTexture = contente.Load<Texture2D>("gBlockGroot");
            this.Position = position;
        }
        static public int TextureWidth
        {
            get
            {
                return 32;
            }
        }
        static public int TextureHeight
        {
            get
            {
                return 32;
            }
        }

        public Rectangle GetCollisionRectangle()
        {
            return new Rectangle((int)this.Position.X, (int)this.Position.Y, this.ObjectTexture.Width, this.ObjectTexture.Height);
        }

        public Color[] GetTextureColorData()
        {
            throw new NotImplementedException();
        }
    }
}
