﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SunnysAdventure.Class.Environment.Tile16x16
{
    class Ondergrond4 : Tile16x16_Object
    {
        public Ondergrond4(ContentManager contente, Vector2 position) : base(position)
        {
            this.ObjectTexture = contente.Load<Texture2D>("ondergrond4");
        }
    }
}
