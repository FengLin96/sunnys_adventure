﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using SunnysAdventure.Class.Collision;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SunnysAdventure.Class.Environment.Tile16x16
{
    class BlockGroen1: Tile16x16_Object, ICollide
    {
        public BlockGroen1(ContentManager contente, Vector2 position) : base(position)
        {
            this.ObjectTexture = contente.Load<Texture2D>("blockGroen1");

        }
        public Rectangle GetCollisionRectangle()
        {
            return new Rectangle((int)this.Position.X, (int)this.Position.Y, this.ObjectTexture.Width, this.ObjectTexture.Height);
        }

        public Color[] GetTextureColorData()
        {
            throw new NotImplementedException();
        }

        
    }  
    class BlockGroen2 : Tile16x16_Object, ICollide
    {
        public BlockGroen2(ContentManager contente, Vector2 position) : base(position)
        {
            this.ObjectTexture = contente.Load<Texture2D>("blockGroen2");

        }
        public Rectangle GetCollisionRectangle()
        {
            return new Rectangle((int)this.Position.X, (int)this.Position.Y, this.ObjectTexture.Width, this.ObjectTexture.Height);
        }

        public Color[] GetTextureColorData()
        {
            throw new NotImplementedException();
        }
    }
    class BlockGroen3 : Tile16x16_Object, ICollide
    {
        public BlockGroen3(ContentManager contente, Vector2 position) : base(position)
        {
            this.ObjectTexture = contente.Load<Texture2D>("blockGroen3");

        }
        public Rectangle GetCollisionRectangle()
        {
            return new Rectangle((int)this.Position.X, (int)this.Position.Y, this.ObjectTexture.Width, this.ObjectTexture.Height);
        }

        public Color[] GetTextureColorData()
        {
            throw new NotImplementedException();
        }
    }
}
