﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SunnysAdventure.Class.Environment
{
     class Tile16x16_Object:EnvironmentObject
    {
        public Tile16x16_Object( Vector2 position)
        {
            this.Position = position;
        }
        static public int TextureWidth
        {
            get
            {
              return 16;
            }
        }
        static public int TextureHeight
        {
            get
            {
                return 16;
            }
        }
    }
}
