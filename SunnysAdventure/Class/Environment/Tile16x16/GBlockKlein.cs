﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using SunnysAdventure.Class.Collision;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SunnysAdventure.Class.Environment
{
    class GBlockKlein : Tile16x16_Object, ICollide
    {
        public GBlockKlein(ContentManager contente, Vector2 position):base(position)
        {
            this.ObjectTexture = contente.Load<Texture2D>("gBlockKlein");
           
        }

        public Rectangle GetCollisionRectangle()
        {
            return new Rectangle((int)this.Position.X, (int)this.Position.Y, this.ObjectTexture.Width, this.ObjectTexture.Height);
        }

        public Color[] GetTextureColorData()
        {
            throw new NotImplementedException();
        }
    }
}
