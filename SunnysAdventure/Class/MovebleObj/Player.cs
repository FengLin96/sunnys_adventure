﻿using Microsoft.Xna.Framework;
using SunnysAdventure.Class.Collision;
using System;
using Microsoft.Xna.Framework.Graphics;
using SunnysAdventure.Class.MovebleObj;
using SunnysAdventure.Class.AnimationClass;
using SunnysAdventure.Class.BedieningClass;
using SunnysAdventure.Class.State;

namespace SunnysAdventure.Class.MovebleObjwa
{
    class Player: MovebleObject
    {

        //private Animation aniWalk, aniCrouch, aniHurt, aniIdle, aniJumpUp,aniJumpDown;
        private WalkState walkState;
        private JumpState jumpState;
        private IdleState idleState;
        private CrouchState crouchState;
        private HurtState hurtState;
        public Bediening bediening { get; set; }
        
        public Player(float horizontalVelocity, float verticalVelocity, float jumpHeight,Vector2 startPosition, Texture2D playerTexture) :base(horizontalVelocity,verticalVelocity,playerTexture)
      {
            bediening = new Bediening();
            Position = startPosition;
            ObjectTexture = playerTexture;
            #region Walk animation toevoegen
            walkState = new WalkState();
            walkState.AddFrame(new Rectangle(1, 700, 38, 45));
            walkState.AddFrame(new Rectangle(41, 700, 42, 46));
            walkState.AddFrame(new Rectangle(85, 700, 38, 45));
            walkState.AddFrame(new Rectangle(125, 700, 38, 45));
            walkState.AddFrame(new Rectangle(165, 700, 36, 46));
            walkState.AddFrame(new Rectangle(203, 700, 38, 45));
            #endregion
            #region Crouch animation toevoegen
            crouchState = new CrouchState();
            crouchState.AddFrame(new Rectangle(127, 37, 42, 39));
            crouchState.AddFrame(new Rectangle(171, 37, 42, 39));
            #endregion
            #region Idle
            
            idleState= new IdleState();
            idleState.AddFrame(new Rectangle(1, 485, 38, 45));
            idleState.AddFrame(new Rectangle(41, 485, 40, 43));
            idleState.AddFrame(new Rectangle(83, 485, 44, 41));
            idleState.AddFrame(new Rectangle(129, 485, 40, 43));
            #endregion
            #region jump
            jumpState = new JumpState(this.VerticalVelocity,130f, new Rectangle(182, 532, 48, 44), new Rectangle(1, 597, 44, 46));
            #endregion
            #region hurt
            hurtState = new HurtState();
            hurtState.AddFrame(new Rectangle(121, 429, 52, 52));
            hurtState.AddFrame(new Rectangle(175, 429, 48, 54));
            #endregion

            CurrentAnimation = idleState.Animation;
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            #region Bediening
            bediening.Update();
            if (bediening.isKeyNotDown)
            {
              idleState.Idle(this);
            }

           
            if (bediening.Left)
            {
                walkState.Walk(this, WalkState.NaarLinks);
            }
            if (bediening.Right)
            {
                walkState.Walk(this, WalkState.naarRechts);
            }
            if (bediening.Jump || jumpState.isJumpProgressNotDone)
            {
                jumpState.Jump(this, gameTime);
            }
            
            //TODO: moet nog uitbreiden
            #endregion
        }
        
    }
}
