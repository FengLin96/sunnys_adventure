﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using SunnysAdventure.Class.AnimationClass;
using SunnysAdventure.Class.Collision;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SunnysAdventure.Class.MovebleObj
{
    class MovebleObject:GameObject, ICollide,IVisible
    {

        private const float gravity = 9.81f;
        private float beginPostitie;
        public float HorizontalVelocity { get; set; }
        public float VerticalVelocity { get; set; }
        public Texture2D ObjectTexture { get; set; }
        public SpriteEffects FlipEffect { get; set; }
        public bool isCollide { get; set; }
        public Animation CurrentAnimation { get; set; }
        public Rectangle IntersectRectangle { get; set; }

        public MovebleObject(float horizontalVelocity, float verticalVelocity, Texture2D objectTexture)
        {
            beginPostitie = Position.X;
            this.HorizontalVelocity = horizontalVelocity;
            this.VerticalVelocity = verticalVelocity;

            FlipEffect = SpriteEffects.None;

            this.ObjectTexture = objectTexture;

            this.isCollide = false;
            this.IntersectRectangle = new Rectangle();
        }

        virtual public void Update(GameTime gameTime)
        {
            CurrentAnimation.Update(gameTime);
        }

        virtual public void Show(SpriteBatch spriteBatch)
        {
            Rectangle destinationRectangle = new Rectangle((int)Position.X, (int)Position.Y, CurrentAnimation.CurrentFrame.SourceRectangle.Width, CurrentAnimation.CurrentFrame.SourceRectangle.Height);
            spriteBatch.Draw(ObjectTexture, destinationRectangle: destinationRectangle, sourceRectangle: CurrentAnimation.CurrentFrame.SourceRectangle, color: Color.AliceBlue, rotation: 0f, origin: new Vector2(0, 0), effects: FlipEffect, layerDepth: 0f);
        }

        public Rectangle GetCollisionRectangle()
        {
            return new Rectangle((int)this.Position.X, (int)this.Position.Y, this.CurrentAnimation.CurrentFrame.SourceRectangle.Width, this.CurrentAnimation.CurrentFrame.SourceRectangle.Height);
        }

        public Color[] GetTextureColorData()
        {
            Color[] textureColorData = new Color[this.ObjectTexture.Width * this.ObjectTexture.Height];
            this.ObjectTexture.GetData(textureColorData);
            return textureColorData;
        }

      
    }
}
