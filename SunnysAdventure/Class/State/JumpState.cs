﻿using Microsoft.Xna.Framework;
using SunnysAdventure.Class.AnimationClass;
using SunnysAdventure.Class.MovebleObj;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SunnysAdventure.Class.State
{
    class JumpState: ActionState
    {
        //public Animation JumpFrame { get; set; }
        // public Animation LandFrame { get; set; } probeer door AantalBewegingenPerSeconde in te stellen

        private float JumpHeight { get; set; }
        private float Velocity { get; set; }

        public bool isJumpProgressNotDone { get; set; }
        private bool hasJumped = false;
        private bool isTop = false;
        private const float gravity = 9.81f;
       
        private float jumpStartTime;
        private float jumpStartPosition;
        private Animation jumpAnimation;
        private Animation landAnimation;

        public JumpState(float velocity, float jumpHeight, Rectangle jumpFrame, Rectangle landFrame)
        {
            //JumpHeight = 130f;
            //Velocity = -7;
            Velocity = velocity;
            JumpHeight = jumpHeight;

            jumpStartTime = 0;
            jumpStartPosition = 0;
            isJumpProgressNotDone = false;

            jumpAnimation = new Animation();
            jumpAnimation.AddFrame(jumpFrame);

            landAnimation = new Animation();
            landAnimation.AddFrame(landFrame);

        }

        public void SetJumpAnimation(Rectangle jumpFrame)
        {
            jumpAnimation.AddFrame(jumpFrame);
        }

        public void SetLandAnimation(Rectangle landFrame)
        {
            landAnimation.AddFrame(landFrame);
        }


        /// <summary>
        /// conditions
        /// 1. not jumped -> jump
        /// 2. Jumped -> land
        /// 3. land -> lager screen
        ///         -> collide -> land op collide object
        /// </summary>
        public void Jump(MovebleObject movebleObject,GameTime gameTime)
        {


            if(!hasJumped && !isTop)
            {
                hasJumped = true;
                isJumpProgressNotDone = true;
                jumpStartTime = 1;
                jumpStartPosition = movebleObject.Position.Y;
                
            }
            if (hasJumped)
            {
               jump(movebleObject, gameTime);
            }
            if (isTop)
            {
                land(movebleObject,gameTime);
            }

        }
        private void jump (MovebleObject movebleObject, GameTime gameTime)
        {
            if (movebleObject.isCollide && movebleObject.GetCollisionRectangle().Top <= movebleObject.IntersectRectangle.Bottom)
            {
                isTop = true;
                hasJumped = false;
                jumpStartTime = 0;
                return;
            }
            movebleObject.CurrentAnimation = jumpAnimation;
            float y = movebleObject.Position.Y + (Velocity/jumpStartTime);

            movebleObject.Position = new Vector2(movebleObject.Position.X, y);
            jumpStartTime += (float)gameTime.ElapsedGameTime.TotalSeconds;

            if (y < jumpStartPosition - JumpHeight) // set isTop true als de top bereikt is
            {
                isTop = true;
                hasJumped = false;
                jumpStartTime = 0;
            }
        }

        private void land(MovebleObject movebleObject, GameTime gameTime)
        {
            if (movebleObject.isCollide && movebleObject.GetCollisionRectangle().Bottom >= movebleObject.IntersectRectangle.Top)
            {
                isTop = false;
                movebleObject.Position = new Vector2(movebleObject.Position.X, movebleObject.Position.Y);
                isJumpProgressNotDone = false;
                return;
            }
            movebleObject.CurrentAnimation = landAnimation;
            float y = movebleObject.Position.Y + (Velocity * -1 + gravity * jumpStartTime);
            movebleObject.Position = new Vector2(movebleObject.Position.X, y);
            jumpStartTime += (float)gameTime.ElapsedGameTime.TotalSeconds;

            //if (movebleObject.Position.Y > jumpStartPosition) // set hasjumped naar false als de bottom breikt is
            //{
            //    isTop = false;
            //    movebleObject.Position = new Vector2(movebleObject.Position.X, jumpStartPosition);
            //    isJumpProgressNotDone = false;
            //}
        }

    }
}
