﻿using Microsoft.Xna.Framework;
using SunnysAdventure.Class.AnimationClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SunnysAdventure.Class.State
{
   abstract class ActionState
    {
        public Animation Animation { get; set; }
        
        public ActionState( )
        {
            Animation = new Animation();
        }

        virtual public void AddFrame(Rectangle rectangle )
        {
            Animation.AddFrame(rectangle);
        }
    }
}
