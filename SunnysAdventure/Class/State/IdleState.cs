﻿using SunnysAdventure.Class.AnimationClass;
using SunnysAdventure.Class.MovebleObj;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SunnysAdventure.Class.State
{
    class IdleState:ActionState
    {
        public IdleState() 
        {

        }
        public void Idle(MovebleObject movebleObj)
        {
            movebleObj.CurrentAnimation = this.Animation;
        }

    }
}
