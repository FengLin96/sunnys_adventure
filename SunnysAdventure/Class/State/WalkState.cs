﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using SunnysAdventure.Class.AnimationClass;
using SunnysAdventure.Class.MovebleObj;

namespace SunnysAdventure.Class.State
{
    class WalkState: ActionState
    {
        public WalkState()
        {
        }


       static public int NaarLinks {get { return (1); } }
       static public int naarRechts{get { return (0);} }

        public void Walk(MovebleObject movebleObject, int richting) // 0 = naar rechts, 1 naar links
        {
            if (richting == 0 )
            {
                if (movebleObject.isCollide && (movebleObject.GetCollisionRectangle().Right >= movebleObject.IntersectRectangle.X))
                {
                    movebleObject.Position = new Vector2(movebleObject.Position.X-10, movebleObject.Position.Y);
                    movebleObject.FlipEffect = SpriteEffects.None;
                    movebleObject.CurrentAnimation = this.Animation;

                }
                else
                {
                    movebleObject.Position += new Vector2(movebleObject.HorizontalVelocity, 0);
                    movebleObject.FlipEffect = SpriteEffects.None;
                    movebleObject.CurrentAnimation = this.Animation;
                }
              

            }
            else if (richting == 1)
            {
                if (movebleObject.isCollide && (movebleObject.GetCollisionRectangle().Left >= movebleObject.IntersectRectangle.Left))
                {
                    movebleObject.Position = new Vector2(movebleObject.Position.X + 5, movebleObject.Position.Y);
                    movebleObject.FlipEffect = SpriteEffects.FlipHorizontally;
                    movebleObject.CurrentAnimation = this.Animation;
                }
                else
                {
                    movebleObject.Position -= new Vector2(movebleObject.HorizontalVelocity, 0);
                    movebleObject.FlipEffect = SpriteEffects.FlipHorizontally;
                    movebleObject.CurrentAnimation = this.Animation;
                }
            }
        }
    }
}
