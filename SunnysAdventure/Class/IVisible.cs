﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SunnysAdventure.Class
{
    interface IVisible
    {
        void Update(GameTime gameTime);
        void Show(SpriteBatch spriteBatch);
    }
}
