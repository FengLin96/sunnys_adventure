﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using SunnysAdventure.Class.Collision;
using SunnysAdventure.Class.Environment;
using SunnysAdventure.Class.MovebleObjwa;
using System;

namespace SunnysAdventure
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        Player player;
        Level1 level1;
        bool isCollideTest = false;
        Rectangle testRectangle;
        Vector2 xy;

        Rectangle backgrountRec;
        Texture2D backgroundTexture;
        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferWidth = 960;
            graphics.PreferredBackBufferHeight = 496;
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
            player = new Player(4f, -7f, 90f, new Vector2(100, 350), Content.Load<Texture2D>("SunnyAdventure"));
            level1 = new Level1(Content);
            level1.CreateWorld();
            testRectangle = new Rectangle();
            xy = new Vector2();

            backgroundTexture = Content.Load<Texture2D>("background");
            backgrountRec = new Rectangle(0, 0, GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height);


        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {

            
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here

            player.Update(gameTime);

            if (!player.isCollide)
            {
                for (int i = 0; i < level1.CollidableObjectList.Count; i++)
                {                   
                            CollisionDetector.RectangleCollisionDetector(player, level1.CollidableObjectList[i]);
                            Console.WriteLine(player.IntersectRectangle.ToString());
                            Console.WriteLine($"right van player x:{player.GetCollisionRectangle().Right}");
                            //if (player.isCollide)
                            //{
                            //    xy = new Vector2(i, j);
                            //    break;
                            //}
                       
                    

                    //if (player.isCollide)
                    //{
                    //    break;
                    //}

                }
            }
            //else
            //{
            //    CollisionDetector.RectangleCollisionDetector(player, level1.BlockBigArrayy[(int)xy.X, (int)xy.Y]);

            //}


            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here
            spriteBatch.Begin();
            spriteBatch.Draw(backgroundTexture, backgrountRec, Color.White);
            player.Show(spriteBatch);
            level1.DrawWorld(spriteBatch);


            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
